var http = require("http");
var mongo = require("mongodb");
var mongoClient = mongo.MongoClient;

var addRecord = function(db, callback) {
	var insertCallback = function(err, result) {
		if(err) {
			console.log("Could not insert record");
			callback();
			return 0;
		}
		console.log("Record added");
		callback();
		return 1;
	};
	var blogRecord = {
		"id": 1,
		"author": "Shruthi",
		"duration": 1000,
		"title": "A Sweet Song",
		"data": [100, 200, 300, 400, 500, 600, 700, 800]
	};
	db.collection('audioblogs').insertOne(blogRecord, insertCallback);
};

var readRecord = function(db, callback) {
	var cursor = db.collection("audioblogs").find({"id": 1});
	cursor.each(function(err, doc) {
		if(err) {
			console.log("Could not find entry");
			callback();
			return 0;
		}
		console.log(doc);
		callback();
		return 1;
	});
};

var deleteRecord = function(db, callback) {
	var deleteCallback = function(err, result) {
		if(err) {
			console.log("Could not delete");
			callback();
			return 0;
		}
		console.log("Deleted one record");
		callback();
		return 1;
	};
	db.collection("audioblogs").deleteOne({"id": 1}, deleteCallback);
};

var server = http.createServer(function(req, resp){
	var dbURL = "mongodb://localhost:27017/test";
	mongoClient.connect(dbURL, function(err, db) {
		if(err) {
			console.log("Could not connect");
			return 0;
		}
		console.log("Connected successfully");
		addRecord(db, function() {
			readRecord(db, function() {
				deleteRecord(db, function() {
					db.close()
				});
			});
		});
		return 1;
	});
	resp.write("hello world!");
	resp.end();
});

server.listen(8000);
